package triggers;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.HXP;
import defines.GV;
import flash.geom.Point;
import levels.Level2;
import src.units.Player;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class EnterInJet extends Entity
{
	var timeout:Float;
	var triggered:Bool;
	
	public function new(x:Float, y:Float) 
	{
		super(x, y);
		graphic = new Image("gfx/jet.png");
		setHitbox(10, 30, -5);
		layer = GV.layerPlayer;
		timeout = 2;
		triggered = false;
	}
	
	override public function update() 
	{
		var e:Entity = collide(GV.playerTag, x, y);
		
		// if player collide
		if (e != null && Std.is(e, Player) && triggered == false) {
			Reflect.callMethod(e, Reflect.field(e, "setVelocityStep"), [0,0]);
			Reflect.callMethod(e, Reflect.field(e, "setVelocityBase"), [0,-2]);
			//Reflect.callMethod(e, Reflect.field(e, "setFrictionAndAccel"), [0, 0.5]);
			Reflect.callMethod(e, Reflect.field(e, "forceSpriteAnim"), [GV.jetAnim, x, y]);
			Reflect.callMethod(e, Reflect.field(e, "setYMin"), [HXP.camera.y -40]);
			
			visible = false;
			triggered = true;
		}
		
		if (triggered == true)
			timeout -= HXP.elapsed;
	
		// launch next level
		if (timeout < 0)
			HXP.scene = new Level2();
	}
	
}