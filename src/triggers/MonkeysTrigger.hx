package triggers;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import src.units.Player;
import src.units.GroundMonkey;
import defines.GV;
import com.haxepunk.Sfx;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class MonkeysTrigger extends Entity
{
	
	private var spawnTimer:Float;
	private var spawnCDValue:Float;
	
	public function new(x:Float, y:Float) 
	{
		super(x, y);
		var sprite = new Spritemap("gfx/burger.png", 10, 30);
		graphic = sprite;
		setHitbox(10, 30);
		layer = GV.layerPlayer;

		// 10 sec before the begin of spawning enemies
		spawnTimer = 10;
		
		//spawn cooldow
		spawnCDValue = 1;
	}

	override public function update() 
	{
		var e:Entity = collide(GV.playerTag, x, y);
		
		// if player collide
		if (e != null && Std.is(e, Player)) {

			// boost player
			Reflect.callMethod(e, Reflect.field(e, "boost"), []);
			
			// and boost enemy pop
			spawnCDValue = 0.1;
			
			// graphic
			graphic = null;
			
			// sfx
			new Sfx("sfx/player_powerup.mp3").play();

		}
		
		// spawn enemies
		spawnTimer -= HXP.elapsed;
        if (spawnTimer < 0)
            spawn();
			
	}
	
	
	private function spawn()
    {
		var row:Int = Math.round( Math.random() * 2);
        var x:Float = HXP.camera.x + HXP.width;
		var y:Float = row * 16 + 56; 
		scene.add(new GroundMonkey(x, y));
		spawnTimer = spawnCDValue;
    }
	
	
}