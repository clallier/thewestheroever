package triggers;

import com.haxepunk.Entity;
import com.haxepunk.HXP;
import elements.Wreck;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class WreckTrigger extends Entity
{
	private var spawnTimer:Float;
	private var timerValue:Float;
	
	public function new(x:Float, y:Float) 
	{
		super(x, y);
		spawnTimer = 5;
		timerValue = 1;
	}
	
	override public function update() 
	{	
		// spawn enemies
		spawnTimer -= HXP.elapsed;
        if (spawnTimer < 0) {
			var x:Float = Math.random()* (HXP.width - 7);
			var y:Float = HXP.camera.y + HXP.height+10;
			scene.add(new Wreck(x, y));
			spawnTimer = timerValue;
			
			if (timerValue > 0.33)
				timerValue *= 0.95;
				
			//HXP.log(timerValue);
		}
	}
}