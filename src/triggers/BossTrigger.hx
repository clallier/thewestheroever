package triggers;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import defines.GV;
import units.Boss;
import src.units.Player;
import src.units.SpaceMonkey;
import levels.Level3;
import com.haxepunk.Sfx;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class BossTrigger extends Entity
{
	private var bossTimer:Float;
	private var spawnTimer:Float;
	private var boss:Boss;
	
	public function new(x:Float, y:Float) 
	{
		super(x, y);
		var sprite = new Spritemap("gfx/burger.png", 10, 30);
		graphic = sprite;
		setHitbox(10, 30);
		layer = GV.layerPlayer;

		// 30 sec before the begin of spawning boss
		bossTimer = 30;
		
		//spawn cooldow
		spawnTimer = 5;
	}
	
	override public function update() 
	{
		var e:Entity = collide(GV.playerTag, x, y);
		
		// if player collide
		if (e != null && Std.is(e, Player)) {

			// boost player
			Reflect.callMethod(e, Reflect.field(e, "boost"), []);
			// boost boss 
			boss.boost();
			
			//sfx
			new Sfx("sfx/player_powerup.mp3").play();

			
			graphic = null;
		}
		
		// spawn enemies until the boss
        if (bossTimer > 0 && spawnTimer < 0)
            spawn();
		else 
			spawnTimer -= HXP.elapsed;	
			
		// spawn boss
        if (bossTimer < 0)
            spawnBoss();
		else if(bossTimer > 0)
			bossTimer -= HXP.elapsed;
	}
	
	private function spawn()
    {
		var x = Math.random()*(HXP.width-this.width);
		scene.add(new SpaceMonkey(x, HXP.camera.y-11));
		spawnTimer = 1;
    }
	
	private function spawnBoss()
    {
		var x = Math.random() * (HXP.width - this.width);
		boss = new Boss(x, HXP.camera.y - 32);
		scene.add(boss);
	
		// stop spawning
		spawnTimer = 0;
		bossTimer = 0;
    }
	
}