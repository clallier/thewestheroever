package ;

import com.haxepunk.Engine;
import com.haxepunk.HXP;
import flash.events.Event;

import levels.Fail;
import levels.Intro;
import levels.Win;

import levels.Level1;
import levels.Level2;
import levels.Level3;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class Main extends Engine
{
	
	override public function init() {
		
		super.init();
		HXP.scene = new Intro ();
		
#if debug
		HXP.console.enable();
#end
	}
	
	public static function main() {
		new Main();
	}
	
	override private function onStage(e:Event = null) {
		super.onStage(e);
		
		this.onResize(new Event("resize"));
		HXP.stage.addEventListener(Event.RESIZE, this.onResize);
	}
	
	
	public function onResize(event:Event) : Void {
		HXP.width = 800;// HXP.stage.stageWidth;
		HXP.height = 480;// HXP.stage.stageHeight;
		HXP.screen.scaleX = HXP.screen.scaleY = 1; // set screen scale to 1x1
		//HXP.resize(HXP.stage.stageWidth, HXP.stage.stageHeight);
	}
}
