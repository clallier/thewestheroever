package utils;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Text;
import com.haxepunk.HXP;
import com.haxepunk.math.Vector;
import flash.geom.Point;
import defines.GV;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class FollowCam extends Entity
{
	// the entity to follow
	private var target:Entity = null;
	
	// permit to choose the scroll axis
	private var isStickedOnY:Bool;
	private var isIncreasing:Bool;
	private	var xMax:Float;
	private	var yMax:Float;
	private	var mapLockX:Float;
	private	var mapLockY:Float;
	
	override public function new(target:Entity, xMax:Float, yMax:Float) 
	{
		this.target = target;
		super(target.x, target.y);
		setScrolling(false, true);
		this.xMax = xMax;
		this.yMax = yMax;
	
		HXP.camera.x = target.x;
		if (target.x > xMax)
			HXP.camera.x = xMax;
			
		HXP.camera.y = target.y;
		if (target.y > yMax)
			HXP.camera.y = yMax;
			
	}
	
	public function lockX(val:Int) {
		HXP.camera.x = val;
		mapLockX = val;
	}
	
	public function lockY(val:Int) {
		HXP.camera.y = val;
		mapLockY = val;
	}
	
	/**
	 * Permit to configure camera scrolling
	 * @param	stickedOnY : follow the Y axis or the X one ?
	 * @param	increasing : or decreasing ?
	 */
	public function setScrolling(stickedOnY:Bool, increasing:Bool) {
		isStickedOnY = stickedOnY;
		isIncreasing = increasing;
	}
	
	public override function update()
	{	
		//HXP.log(HXP.camera.x + " " + HXP.camera.y + " HXP:" +HXP.width + " " +HXP.heigh);
		
		if (target != null) {
			// if camera should stick target on Y 
			if(isStickedOnY == true )
				followOnY();
			
			// else if camera should stick on X
			else
				followOnX();
		}
		super.update();
	}
	
	// Check horizontal axis
	private function followOnX() 
	{
		if (isIncreasing && target.x+HXP.width < xMax)
			HXP.camera.x = HXP.lerp(HXP.camera.x, target.x-10);
		else if (isIncreasing == false && HXP.camera.x > 0)
			HXP.camera.x = HXP.lerp(HXP.camera.x, target.x+90);
		
		// recenter after a shake
		if (HXP.camera.y != mapLockY)
			HXP.camera.y = HXP.lerp(HXP.camera.y, mapLockY);
	}
	
	private function followOnY() 
	{		
		if (isIncreasing && target.y+HXP.height< yMax)
			HXP.camera.y = HXP.lerp(HXP.camera.y, target.y);
		else if (isIncreasing == false && HXP.camera.y > 0)
			HXP.camera.y = HXP.lerp(HXP.camera.y, target.y-90);
		
		// recenter after a shake
		if (HXP.camera.x != mapLockX)
			HXP.camera.x = HXP.lerp(HXP.camera.x, mapLockX);
		
	}
}