package utils;
import com.haxepunk.Entity;
import com.haxepunk.HXP;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class CameraVibrator extends Entity
{
	private var time:Float = 0;
	
	/**
	 * 
	 * @param	time : time to vibrate in s
	 */
	public function vibrate (time:Float) 
	{
		this.time = time;
	}
	
	override public function update() {
		if (time > 0) {
			HXP.camera.y += Math.random()*4 -2;
			HXP.camera.x += Math.random()*4 -2;
			time -= HXP.elapsed;
		} 
	}
}