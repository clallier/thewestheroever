package utils;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Emitter;
import flash.display.BitmapData;
import com.haxepunk.utils.Ease;
import defines.GV;
import com.haxepunk.graphics.Image;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Explosion extends Entity
{

	private var emitter:Emitter;
	
	override public function new() 
	{
		super();
		emitter = new Emitter (new BitmapData(2, 2, false, 0xFFFFFF), 2, 2);
		
		// PARAMS :
		// 1: the type
		// 2: the initial angle
		// 3: the distance each particle will travel during it’s life
		// 4: the time to live of our particle (in secs) 
		// 5: the angle range (360 means it emits from the initial angle to 360)
		// 6: the distance range(in px?) 
		// 7: the time range (in s)
		// 8: the Ease method
		
		// base explosion
		emitter.newType("explosion", [0]);
		emitter.setMotion("explosion", 0, 50, 1, 360, -50, 1, Ease.quadOut);
		emitter.setAlpha("explosion", 1, 0.1);
		
		// smoke
		emitter.newType("smoke", [0]);
		emitter.setMotion("smoke", 60, 90, 0.5, 120, -150, 0.4, Ease.quadOut);
		emitter.setColor("smoke", 0xff0000, 0x000000);
		emitter.setAlpha("smoke", 0.8, 0.1);
	
		graphic = emitter;
		layer = GV.layerBullets;
	}
	
	public function explode(x:Float, y:Float, particles:UInt = 20 )
	{
		for (i in 0...particles) {
			emitter.emit("explosion", x, y);
		}
	}
	
	public function smoke(x:Float, y:Float)
	{
		emitter.emit("smoke", x, y);
	}
	
}