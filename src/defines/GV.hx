package defines;
import com.haxepunk.Sfx;
import gui.HUD;
import utils.CameraVibrator;
import utils.Explosion;

/**
 * Global Values
 * @author Godjam - QilinEggs
 */
class GV
{
	// explosions generator
	public static var explosion:Explosion =	new Explosion();
	// camera shaker
	public static var camVibrator:CameraVibrator = new CameraVibrator();
	// HUD
	public static var hud:HUD = new HUD(5, 0);
	
	// tile size
	public inline static var k:Int = 16;
	public static var music:Sfx;
	
	// layers
	public inline static var layerBackground:Int = 5;
	public inline static var layerBullets:Int = 4;
	public inline static var layerPlayer:Int = 3;
	public inline static var layerEnemy:Int = 2;
	public inline static var layerBoss:Int = 1;
	public inline static var layerGui:Int = 0;
	
	// collision tags
	public inline static var enemyTag:String = "enemy";
	public inline static var laserTag:String = "laser";
	public inline static var playerTag:String = "player";
	
	// player animation 
	public inline static var idleAnim:String = "idle";
	public inline static var walkAnim:String = "walk";
	public inline static var jetAnim:String = "jet";
	public inline static var flyAnim:String = "fly";
	
	// text 
	public inline static var textColor:Int = 0x222222; //0xd5d5d5
}