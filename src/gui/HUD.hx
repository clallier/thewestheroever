package gui;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.graphics.Text;
import com.haxepunk.utils.Draw;
import defines.GV;
import flash.display.BitmapData;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class HUD extends Entity
{

	private var texture:Stamp;
	private static var hpCurHero:Int = 10;
	private static var score:Int = 0;
	private var hpCurBoss:Int;
	private var sunglasses:Bool;
	private var princess:Bool;
	
	override public function added () 
	{
		super.added();
		this.hpCurBoss = -1;
		updateGraphic(hpCurHero, score, hpCurBoss);
		layer = GV.layerGui;
		
		sunglasses = false;
		princess = false;
	}
	
	public function incrementScore(incr:Int)
	{
		if (incr == 0)
			return;
			
		score += incr;
		updateGraphic(hpCurHero, score, hpCurBoss);
	}
	
	public function updateLife(hpCur:Int)
	{
		// if hp haven't changed
		if (HUD.hpCurHero == hpCur)
			return;
			
		updateGraphic(hpCur, score, hpCurBoss);
		
		// save hp value
		HUD.hpCurHero = hpCur;
	}
	
	
	public function updateBossLife(hpCur:Int)
	{
		// if hp haven't changed
		if (this.hpCurBoss == hpCur)
			return;
			
		updateGraphic(hpCurHero, score, hpCur);
		
		// save hp value
		this.hpCurBoss = hpCur;
	}
	
	public function resetScore() 
	{
		score = 0;
		princess = false;
		sunglasses = false;
	}
	
	public function setGlasses(bool:Bool) 
	{
		sunglasses = bool;
	}
	
	public function setPrincess(bool:Bool) 
	{
		princess = bool;
	}
	
	public function getScore() 
	{
		return score;
	}
	
	public function gotPrincess() 
	{
		return princess;
	}
	
	public function gotSunglasses() 
	{
		return sunglasses;
	}
	
	private function updateGraphic(hpCur1:Int, score:Int, hpCur2:Int) {
		
		var w = 50, h = 5, x = 14, y=3;
		var buffer:BitmapData = new BitmapData(2*w+x+70, h+y+2, true, 0);
		Draw.setTarget(buffer);
		
		//life
		Draw.text("life", 0, 0, {size:8, color:0xd5d5d5});
		drawLifeBar(x, y, w, h, hpCur1, 10, 0x16e032);
		
		// score 
		Draw.text("score:" + score, 70, 0, {size:8, color:0xd5d5d5});
		
		if (hpCur2 != -1) {
			// set offset for drawing
			x = 130;
			drawLifeBar(x, y, w, h, hpCur2, 100, 0xff0000);
		}
		
		texture = new Stamp(buffer);
		texture.scrollX = texture.scrollY = 0;
		graphic = texture;
	}
	
	function drawLifeBar(x:Int, y:Int, w:Int, h:Int, lifeCur:Int, lifeMax:Int, color:Int)
	{
		if (lifeMax == 0)
			return;
			
		if (lifeCur > lifeMax)
			lifeCur = lifeMax;
		
		// shadow rect
		//down
		Draw.line(x+1, 	y+h+1, x+w-2, y+h+1, 0xb3b2b2);
		//left
		Draw.line(x, 	y+2, x,   y+h, 0xb3b2b2);
		//right
		Draw.line(x+w-1,y+2, x+w-1,y+h, 0xb3b2b2);
		
		
		// outline rect
		//top
		Draw.line(x+1, 	y, 	 	x+w-2, 	y, 0xd5d5d5);
		//down
		Draw.line(x+1, y+h, 	x+w-2, 	y+h, 0xd5d5d5);
		//left
		Draw.line(x, 	y+1, 	x,   	y+h-1, 0xd5d5d5);
		//right
		Draw.line(x+w-1, y+1, 	x+w-1, 	y+h-1, 0xd5d5d5);
		
		// life bar
		if(lifeMax != 0) {
			var p = Std.int(lifeCur / lifeMax * w) - 2;
			if (p < 0)	p = 0;
			Draw.rect(x+1, y+1, p, h-1, color);
		}
	}
	
	
}