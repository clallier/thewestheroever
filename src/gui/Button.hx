package gui;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Text.TextOptions;
import flash.display.BitmapData;
import com.haxepunk.utils.Draw;
import com.haxepunk.graphics.Stamp;
import defines.GV;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Button extends Entity
{

	private var w:Int;
	private var h:Int;
	private var txt:String;
	private var normal:Stamp;
	private var hover:Stamp;
	private var func:Dynamic =null;
	private var params:Dynamic =null;
	
	public function new(txt:String, x:Int, y:Int, w:Int=32, h:Int=10, func:Dynamic=null, params:Dynamic=null) 
	{
		super(x, y);
		this.w = w;
		this.h = h;
		this.txt = txt;
		normal = drawButtonStamp(0xdcd3d3, 0x16e032, 0xffffff);
		normal.scrollX = normal.scrollY = 0;
		hover = drawButtonStamp(0xdcd3d3, 0xff9c00, 0xffffff);
		hover.scrollX = hover.scrollY = 0;
		
		this.func = func;
		this.params = params;
				
		graphic = normal;

		
		setHitbox(w, h);
		layer = GV.layerGui;
	}
	
	private function drawButtonStamp(shadowColor:Int, outlineColor:Int, bgColor:Int):Stamp 
	{
		var picture:BitmapData = new BitmapData(w, h+1, true, 0);
		Draw.setTarget(picture);
		
		// shadow rect
		Draw.rect(0, 0, w, h, shadowColor, 1);
		// outline rect
		Draw.rect(0, 0, w, h, outlineColor, 1);
		// bg rect
		Draw.rect(1, 1, w-2, h-2, bgColor, 1);
		//text
		Draw.text(txt, 0, 0, {size:8, color:GV.textColor});
		
		return new Stamp(picture);
	}
	
	
	override public function update() 
	{
		//HXP.log(this.x +" "+ this.y + " vs " +Std.int(HXP.screen.mouseX) + " " + Std.int(HXP.screen.mouseY));
		// on mouse hover
		if(collidePoint(x, y, Std.int(Input.mouseX), Std.int(Input.mouseY))) {
				
			graphic = hover;
			//HXP.log("hover!");
			
			if(Input.mouseReleased)
			{
				//trigger the action
				if (func != null) {
					if (params == null) params = [];
					Reflect.callMethod(scene, Reflect.field(scene, func), params);
				}	
			}
		}
		
		// no mouse hover
		else
			graphic = normal;
	}
}