package gui;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap.CallbackFunction;
import com.haxepunk.graphics.Text;
import com.haxepunk.HXP;
import defines.GV;
import flash.text.TextFormatAlign;
import com.haxepunk.graphics.Image;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class InfoText extends Entity
{

	private var ttl:Float;
	private var t:String;
	private var triggered:Bool;
	private var func:Dynamic;
	private var params:Dynamic;
	
	/**
	 *  
	 * A centered text, trigered by collision with a time to be displayed
	 * @param	t : the text to display
	 * @param	x : the hitbox x-origin in tiles (automaticaly scaled by GV.k) 
	 * @param	y : the hitbox y-origin in tiles (automaticaly scaled by GV.k)
	 * @param	func : a callback to call on the trigger, have to be a scene method name
	 * @param	param : a list of dynamics to pass to the function to call
	 * @param	time : the time to live in s. Default : 3s
	 * @param	w : the hitbox width in px. Default : scene width
	 * @param	h : the hitbox height in px. Default : scene height
	 */
	public function new(t:String, x:Float=-1, y:Float=-1, func:String=null, params:Array<Dynamic>=null, time:Float=3, w:Int=-1, h:Int=-1) 
	{
		if (x == -1) x = HXP.camera.x;
		if (y == -1) y = HXP.camera.y;
		super(x* GV.k, y *GV.k);
		if (w == -1) w = HXP.width;
		if (h == -1) h = HXP.height;
		setHitbox(w, h);
		//graphic = Image.createRect(w, h, 0x00ff00); //debug rect
		this.ttl = time;
		this.t = t;
		this.triggered = false;
		
				
		this.func = func;
		this.params = params;
	}
	
	override public function update() {
		//collision test
		collisionTest();
		
		if(triggered)
			ttl -= HXP.elapsed;
		
		// deletion 
		if (ttl < 0)
			scene.remove(this);
			
		super.update();
	}
	
	private function collisionTest() {
		var e:Entity = collide(GV.playerTag, x, y);
		
		if (e != null) {
			triggered = true;
			var text:Text = new Text(t);
			// center text
			x = (HXP.halfWidth)-(text.width/2);
			y = (HXP.halfHeight)-(text.height/2);
			text.scrollX = text.scrollY = 0;
			graphic = text;
			layer = GV.layerGui;
			
			// trigger callback
			if (func != null) {
				if (params == null) params = [];
				Reflect.callMethod(scene, Reflect.field(scene, func), params);
			}
		}
	}
	
}