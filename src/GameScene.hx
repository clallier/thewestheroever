package ;

import com.haxepunk.HXP;
import com.haxepunk.Scene;
import defines.GV;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class GameScene extends Scene
{	
	
	static var t:Float = 0;
	// init minimal world
	override public function begin() 
	{
		// add explosion emitter
		add(GV.explosion);
		
		// add cam vibrator
		add(GV.camVibrator);
		
		// add HUD
		add(GV.hud);
		
		//camera config 
		HXP.screen.scale = 4;
		//FPS
		HXP.assignedFrameRate = 60;
		
		super.begin();
	}
	
	override public function end() 
	{
		remove(GV.explosion);
		remove(GV.camVibrator);
		remove(GV.hud);
		super.end();
	}
	
	override public function update() {
		super.update();
		
		//HXP.log(t += HXP.elapsed);
	}
}