package elements;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.HXP;
import src.units.Enemy;
import src.units.Player;
import defines.GV;
import com.haxepunk.Sfx;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Laser extends Entity
{

	// Time to live (in s) 
	private var ttl:Float = 1;
	private var xDir:Int;
	private var yDir:Int;
	private var lenght:Int;
	
	public function new(x:Float, y:Float, xDir:Int = 1, yDir:Int = 0, lenght:Int=64 ) 
	{
		super(x, y);
		this.xDir = xDir;
		this.yDir = yDir;
		this.lenght = lenght;
		
		// force vertical / horizontal laser
		if (xDir != 0)
			yDir = 0;
		
		var w:Int = lenght;
		var h:Int = 4;
		
		if (yDir != 0) {
			w = 4;
			h = lenght;
		}
			
		var i:Image = Image.createRect(w, h, 0xFF0000, 0.7);
		graphic = i;
		layer = GV.layerBullets;
		
		setHitbox(w, h);
		type = GV.laserTag;
	}
	
	private function collideTest()
    {
		var list:Array<String> = [GV.enemyTag, GV.playerTag];
		var e:Entity = collideTypes(list, x, y);
        
		// if e is an enemy, hurt it
		if (e != null && Std.is(e, Enemy)) {
			Reflect.callMethod(e, Reflect.field(e,"hit"), []);
		}
		
		if (e != null && Std.is(e, Player)) {
			Reflect.callMethod(e, Reflect.field(e, "hit"), []);
			scene.remove(this); // emove itself
		}
		
		return true;
    }
	
	public function updateTimeToLive() : Void
    {
        ttl -= HXP.elapsed;
		
		if(ttl < 0)
			scene.remove(this);
    }
 
    public override function update()
    {
		// horizontal laser
		var mx:Float = 0.2 * lenght * xDir; // speed depends of lenght and dir
		var my:Float = 0;
		
		// vertical 
		if (yDir != 0) {
			mx = 0;
			my = 0.2 * lenght * yDir;
		}
		
		updateTimeToLive();
		
		moveBy(mx, my);
        collideTest();
		super.update();
    }
	
}