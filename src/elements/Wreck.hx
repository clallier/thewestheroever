package elements;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.Sfx;
import defines.GV;
import src.units.Player;
import com.haxepunk.HXP;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Wreck extends Entity
{

	// time to live
	private var ttl:Float;
	private var smokeCounter:Int;
	
	public function new(x:Float, y:Float) 
	{
		super(x, y);
		var i = Math.round(Math.random() * 2);
		if(i == 0)
			graphic = new Image("gfx/wreck.png");
		else 
			graphic = new Image("gfx/wreck2.png");
		
		setHitboxTo(graphic);
		layer = GV.layerEnemy;
		ttl = Math.random() * 2 + 2;
		smokeCounter = 0;
	}
	
	override public function update() {
		moveBy(0, 2, GV.playerTag);
		
		if(++smokeCounter %4 == 0)
			GV.explosion.smoke(x + 3, y);
			
		ttl -= HXP.elapsed;
		
		// destroy 
		if (ttl <= 0) {
			scene.remove(this);
			GV.explosion.explode(x, y);
			GV.hud.incrementScore(50);
			new Sfx("sfx/wreck_explosion.mp3").play(0.5);
		}
		
	}
	
	override public function moveCollideY(e:Entity):Bool {
		// if e is an player, hurt it
		if (e != null && Std.is(e, Player)) {
			Reflect.callMethod(e, Reflect.field(e, "hit"), [2]);
			ttl = 0;
			return true;
		}
		return false;
	}
}