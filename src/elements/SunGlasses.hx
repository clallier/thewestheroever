package elements;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import flash.geom.Rectangle;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import defines.GV;
import gui.HUD;
import src.units.Player;
import utils.Color;
import gui.InfoText;
import com.haxepunk.Sfx;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class SunGlasses extends Entity
{

	private var hero:Entity;
	
	public function new(x:Float=0, y:Float=0) 
	{
		super(x, y);
		var image:Image = new Image("gfx/sunGlasses.png", new Rectangle(0,0,9, 4));
		setHitbox(9, 4);
		// graphic
		graphic = image;
		layer = GV.layerEnemy; // over the hero
	}
	
	
	override public function update() {
		
		// no hero 
		if(hero == null) { 
			moveBy(0, 2.5, GV.playerTag);	
		}
		
		// destroy 
		if (y <= HXP.camera.y - 10) {
			scene.remove(this);
			GV.explosion.explode(x, y); 
			scene.add(new InfoText("Bad! \n-5 in charisma!", 0, y / GV.k));
		}
		
	}
	
	override public function moveCollideY(e:Entity):Bool {
		// we collide with the player
		if (e != null) {
			
			// Change sky color 
			Color.randomSkyColor();
			
			// up score
			GV.hud.incrementScore(3000); // you got the sunglasses !

			// text 
			scene.add(new InfoText("AWESOME!", 0, y / GV.k));

			// sfx 
			new Sfx("sfx/awesome.mp3").play();
			
			// release the graphic
			scene.remove(this);
			
			// save it 
			GV.hud.setGlasses(true);
			
			return true;
		}
		return false;
	}
}