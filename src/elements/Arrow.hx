package elements;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import defines.GV;
import com.haxepunk.HXP;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Arrow extends Entity
{

	public function new(x:Float=0, y:Float=0) 
	{
		super(x, y);
		var i:Image = new Image("gfx/arrow.png");
		graphic = i;
		layer = GV.layerGui;
	}
	
	override public function update() 
	{
		if(x < 35)
			x = HXP.lerp(x, 35, 0.005);
		
		y = HXP.camera.y + HXP.height -4;
	}
	
}