package elements;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Backdrop;
import com.haxepunk.graphics.Graphiclist;
import com.haxepunk.graphics.Tilemap;
import defines.GV;

/**
 * Ground - a background entity, just draw the background of the first scene
 * @author Godjam - QilinEggs
 */
class Ground extends Entity
{
	public function new(x:Float, y:Float, w:Float, h:Float) 
	{
		super(x, y);
		
		//bg 0
		var bg:Backdrop = new Backdrop("gfx/bg0.png", true, false);
		bg.scrollX = 0.1; //TODO
		addGraphic(bg);
		
		//bg 1
		bg = new Backdrop("gfx/bg1.png", true, false);
		bg.y = 16; //TODO
		bg.scrollX = 0.2; //TODO
		addGraphic(bg);
		
		// ground
		var tiles:Tilemap = new Tilemap("gfx/GroundTiles.png", Std.int(w) * 16, 480, 16, 16);
		tiles.setRect(0, 4, Std.int(w), Std.int(h), 2); //TODO
		addGraphic(tiles);
		
		layer = GV.layerBackground;
	}
	
}