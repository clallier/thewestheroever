package elements;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Backdrop;
import com.haxepunk.HXP;
import defines.GV;
import utils.Color;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class DarkSky extends Entity
{
	var cd:Float;
	var forced:Bool;
	var color:Int;
	
	public function new(x:Float, y:Float, w:Float, h:Float) 
	{
		super(x, y);
		var k:Int = GV.k;// tilesize
		
		//bg 0
		var bg:Backdrop = new Backdrop("gfx/stars1.png", true, true);
		bg.scrollY = 1.6;
		addGraphic(bg);
		
		//bg 1
		bg = new Backdrop("gfx/stars2.png", true, true);
		bg.scrollY = 0.16;
		addGraphic(bg);
		
		cd = 3;
		forced = true;
		color = 0xff000000;
	}
	
	
	override public function update()
	{
		if (cd < 0) {
			Color.fadeSkyColor(color);
			cd = 1;
		}
		
		if (HXP.screen.color == color)
			return;
			
		cd -= HXP.elapsed;
	}

	public function setColor(forceColor:Bool, color:Int = 0xff000000) 
	{
		this.forced = forceColor;
		this.color = color;
	}
	
}