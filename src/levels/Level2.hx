package levels;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Text;
import com.haxepunk.HXP;
import com.haxepunk.Sfx;
import defines.GV;
import elements.DarkSky;
import flash.geom.Point;
import gui.InfoText;
import src.units.Enemy;
import src.units.Player;
import src.units.SpaceMonkey;
import triggers.BossTrigger;
import utils.FollowCam;
import utils.Color;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level2 extends GameScene
{
	private var sky:DarkSky;
	private var player:Player;
	private var followCam:FollowCam;
	private var timeout:Float;
	private var destroyed:Bool;
	private var playerExploded:Bool; 
	private var monkeysExploded:Bool;
	private var alarm:Sfx;
	
	override public function begin() 
	{
		super.begin();
		destroyed = false;
		playerExploded = false; 
		monkeysExploded = false;
		
		//world size (in tiles)
		var size = new Point(12.5, 300);
		
		// add background + world size
		sky = new DarkSky(0, 20, size.x, size.y);
		add(sky);
		
		// add player + passing walkable limits (in px)
		player = new Player(6 * GV.k, size.y * GV.k,		// origin pos
							0, 0, 							// min limits
							size.x * GV.k, size.y * GV.k); 	// max limits
		player.setVelocityStep(2, 0);
		player.setVelocityBase(0, -1);
		player.forceSpriteAnim(GV.jetAnim);
		add(player);
		
		// add follow cam and stick it to the player + passing world size (in px)
		followCam = new FollowCam(player, size.x * GV.k, size.y * GV.k);
		followCam.setScrolling(true, false);
		followCam.lockX(0);
		add(followCam);
		
		// boss trigger
		add(new BossTrigger(6 * GV.k, 110 * GV.k));
		
		//texts
		add(new InfoText("Level 2\n Fight the Boss\n and rescue the\n princess", 0, 270));
		add(new InfoText("HURRY UP!", 0, 90));
		add(new InfoText("FASTER!", 0, 65, "playAlarm"));
		
		//alarm sound
		alarm = new Sfx("sfx/alarm.mp3");
		
		//timeout before the end
		timeout = 4;
	}
	
	function playAlarm()
	{
		alarm.play();
	}
	
	function explodePlayer():Void 
	{
		GV.explosion.explode(player.x, player.y);
		player.visible = false;
		new Sfx("sfx/big_explosion.mp3").play();
		playerExploded = true;	
	}
	
	function explodeMonkeys() 
	{
		// destroy all enemies
		var monkeys: Array<Enemy> = new Array<Enemy>();
		getClass(SpaceMonkey, monkeys);
			
		for (m in monkeys)
			m.hit();

		sky.setColor(true, 0xffffffff);
		
		
		//woops
		var text:Text = new Text("Wooooops", 0, 0, 32, 12, { size:14, color:0x0 } );
		text.scrollX = text.scrollY = 0;
		var tx = (HXP.halfWidth)-(text.width/2);
		var ty = (HXP.halfHeight)-(text.height/2);
		var textEntity = new Entity(tx, ty, text);
		textEntity.layer = GV.layerGui;
		add(textEntity);
		
		monkeysExploded = true;
	}
	
	public function gotoLevel3() 
	{
		destroyed = true;		
		alarm.stop();
		
		// destroy camera
		GV.camVibrator.vibrate(4);
		
		// mute player
		player.muteLaser(true);
	}
	
	override public function update() 
	{
		super.update();
		
		if (destroyed)
			timeout -= HXP.elapsed;
		
		if(timeout < 3.5 && monkeysExploded == false)
			explodeMonkeys();
		
		if(timeout < 3 && playerExploded == false)
			explodePlayer();

		if (timeout < 0)
			HXP.scene = new Level3();
		
	}
	

}