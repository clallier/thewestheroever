package levels;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Backdrop;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.graphics.Text;
import com.haxepunk.graphics.Tilemap;
import defines.GV;
import GameScene;
import gui.Button;
import src.units.Player;
import units.Princess;
import com.haxepunk.HXP;
import com.haxepunk.graphics.TiledSpritemap;
import flash.text.TextFormatAlign;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Win extends GameScene
{
	var texts:Array<String>;
	var textEntity:Entity;
	var btnEntity:Entity;
	var cloudEntity:Entity;
	var birdEntity:Entity;
	var i:Int;
	var timeout:Float;
	
	public function new() 
	{
		super();
		super.begin();
		HXP.screen.color = 0x38e1e9;//0xe5f0f2;
		
		//texts 
		texts = new Array<String>();
		texts.push("You got the win");
		texts.push("Awesome!");
		texts.push("A tribute to Trosh\n (Stabyourself.net)");
		texts.push("A game by Godjam");
		texts.push("Music by Yacht\n \"Beam Me Up\"");
		texts.push("www.qilineggs.com");
		texts.push("thank you for playing");
		i = 0;
		timeout = 0;
		
		// cloud
		cloudEntity = new Entity(20, 10, new Image("gfx/cloud.png"));
		add(cloudEntity);
		
		// house
		add(new Entity(0, 10, new Image("gfx/house.png")));
		
		// birds
		var birdSprite:Spritemap = new Spritemap("gfx/birds.png", 8, 8);
		birdSprite.add(GV.flyAnim, [0,1], 1);
		birdEntity = new Entity(20, 15, birdSprite);
		birdSprite.play(GV.flyAnim);
		add(birdEntity);
		
		// text 
		textEntity = new Entity(0, 6);
		add(textEntity);
		
		var player:Player = new Player(100, 55, 100, 30, 100, 30);
		player.setVelocityStep(0, 0);
		player.muteLaser(true);
		add(player);
		
		if (GV.hud.gotPrincess()) {
			add(new Entity(107, 45, new Image("gfx/heart.png")));
			var princess:Princess = new Princess(110, 55);
			princess.active = false;
			princess.setAnim(GV.idleAnim);
			add(princess);
		} else {
			texts.push("Hey! \nwhere is the princess?");
			texts.push("Er! It seems you\nmissed something");
			texts.push("In my view \nyou should retry");
		}
		
		if (GV.hud.gotSunglasses()) {
			player.setSunglasses();
			texts.push("Nice sunglasses!");
		}
			
			
		// water anim
		var t = new TiledSpritemap("gfx/water.png", 16, 16, 208, 64);
		t.add(GV.idleAnim, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], 2);
		t.play(GV.idleAnim);
		var water = new Entity(0, 80, t);
		water.layer = GV.layerBoss;
		add(water);
		
		// restart button 
		btnEntity = new Button("Restart", 150, 100, 36, 12, "restart");
		btnEntity.visible = false;
		add(btnEntity);
	}
	
	override public function update() 
	{
		timeout -= HXP.elapsed;
		// force cam pos
		HXP.setCamera(0, 0);
	
		if (timeout < 0) {
			changeText();
			timeout = 5;
		}
		
		moveCloudAndBirds();
		
		super.update();
	}
	
	function moveCloudAndBirds() 
	{
		cloudEntity.moveBy( -0.1, 0);
		if (cloudEntity.x < -10)
			cloudEntity.x = 220;
	
		birdEntity.moveBy( 0.08, 0);
		if (birdEntity.x > 220)
			birdEntity.x = -10;
	}
	
	function changeText() 
	{
		if (i >= texts.length) {
			i = 0;
			btnEntity.visible = true;
		}
		
		var t:Text = new Text(texts[i]);
		textEntity.graphic = t;
		textEntity.x = 200 - t.width;
	
		++i;
	}
	
	function restart() 
	{
		HXP.scene = new Level1();
	}
	
}