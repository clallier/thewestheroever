package levels;
import com.haxepunk.HXP;
import com.haxepunk.Sfx;
import defines.GV;
import elements.DarkSky;
import elements.SunGlasses;
import flash.geom.Point;
import gui.InfoText;
import src.units.Player;
import triggers.WreckTrigger;
import units.Princess;
import utils.FollowCam;
import elements.Arrow;
import levels.Win;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level3 extends GameScene
{
	var player:Player;
	var sky:DarkSky;
	var wreckTrigger:WreckTrigger;
	
	public function new() 
	{
		super();
		super.begin();
		
		//world size (in tiles)
		var size = new Point(12.5, 500);
		
		// add background + world size
		sky = new DarkSky(0, 20, size.x, size.y); 
		add(sky);
		
		// add player + passing walkable limits (in px)
		player= new Player(6 * GV.k, 0,		// origin pos
							0, 0, 							// min limits
							size.x * GV.k, size.y * GV.k); 	// max limits
		player.setVelocityStep(2, 0);
		player.setVelocityBase(0, 2);
		player.setFrictionAndAccel(0.02, 0.08);
		player.forceSpriteAnim(GV.flyAnim);
		player.muteLaser(true);
		add(player);
		
		// add follow cam and stick it to the player + passing world size (in px)
		var followCam:FollowCam = new FollowCam(player, size.x * GV.k, size.y * GV.k);
		followCam.setScrolling(true, true);
		followCam.lockX(0);
		add(followCam);
		
		// add wreck trigger
		wreckTrigger = new WreckTrigger(0, 0);
		add(wreckTrigger);
		
		//texts 
		add(new InfoText("Level 3\n Beware to the ship\n wrecks", 0, 20));
		add(new InfoText("Catch the princess !", 0, 100, "spawnPrincess"));
		add(new InfoText("Catch the sunglasses !", 0, 200, "spawnSunGlasses"));
		add(new InfoText("Prepare for landing !", 0, 370, "transition"));
		add(new InfoText("", 0, 420, "shake"));
		add(new InfoText("", 0, 499, "gotoEnding"));
		
		HXP.screen.color = 0xffffff;
	}
	
	function spawnPrincess():Void 
	{
		// add princess
		add(new Princess(Math.random() * 12.5 * GV.k, HXP.camera.y + HXP.height *2));
	}
	
	function spawnSunGlasses():Void 
	{
		add(new SunGlasses(Math.random() * 12.5 * GV.k, HXP.camera.y+ HXP.height *2));
	}
	
	function transition():Void 
	{
		new Sfx("sfx/alarm.mp3").play();
		sky.setColor(true, 0xff38e1e9);
		wreckTrigger.active = false;
		add(new Arrow(0, 0));
	}

	function shake():Void 
	{
		GV.camVibrator.vibrate(5);
	}
	
	function gotoEnding():Void 
	{
		var score:Int = Math.floor((100 - Math.abs(HXP.halfWidth - player.x)) * 50);
		GV.hud.incrementScore(score);
		new Sfx("sfx/big_explosion.mp3").play();
		HXP.scene = new Win();
	}	
}