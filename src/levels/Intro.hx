package levels;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Text;
import com.haxepunk.HXP;
import com.haxepunk.Sfx;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Scene;
import utils.Color;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Intro extends Scene
{
	var cartridge:Entity;
	var running:Bool;
	
	public function new() 
	{
		super();
		super.begin();
		HXP.screen.scale = 4;
		
		var img:Image = new Image("gfx/cartridge.png");
		cartridge = new Entity(100 - img.width / 2, -img.height, img);
		add(cartridge);

		// any key listener
		Input.define("start", 	[Key.ANY]);
		
		// scene state
		running = true;
	}
	
	override public function update() 
	{
		var yTarget:Float = 33;
		cartridge.y = HXP.lerp(cartridge.y, yTarget, 0.08);
		
		if (running && yTarget - cartridge.y < 2) {
			new Sfx("sfx/boot.mp3").play();
			add(new Entity(40, 10, new Text("Press any key")));
			running = false;
		}
		
		if (Input.released("start")) 
			HXP.scene = new Level1();
		
		super.update();
	}
	
}