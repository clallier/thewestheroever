package levels;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Text;
import com.haxepunk.Scene;
import com.haxepunk.HXP;
import gui.Button;
import gui.InfoText;
import defines.GV;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Fail extends Scene
{

	public function new() 
	{
		super();
		HXP.screen.color = 0x0;
		
		add(new Entity(10, 5, new Text("Sorry,\nbut our princess \nis in another castle!")));
		add(new Entity(15, 60, new Text("A game by Godjam")));
		add(new Entity(15, 75, new Text("Music by YACHT")));
		add(new Entity(0, 100, new Text("www.qilineggs.com")));
		add(new Button("Retry", 150, 105, 32, 10, "retry"));
		
		// TODO find a nice background
	}
	
	function retry() {
		HXP.scene = new Level1();
	}
	
}