package levels;
import com.haxepunk.HXP;
import com.haxepunk.Sfx;
import defines.GV;
import elements.Ground;
import flash.geom.Point;
import gui.InfoText;
import src.units.Player;
import triggers.EnterInJet;
import triggers.MonkeysTrigger;
import utils.FollowCam;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level1 extends GameScene
{
	private var player:Player;
	private var followCam:FollowCam;
	private var trigger:MonkeysTrigger;
	//private var music:Sfx;
	
	override public function begin() 
	{
		super.begin();
		GV.hud.resetScore();
		
		//world size (in tiles)
		var size = new Point(250, 3);
		
		// add background + world size
		add(new Ground(0, 0, size.x, size.y));
		
		
		// add player + passing walkable limits (in px)
		player = new Player(0, 72, 			// origin pos, y = 4*16 +8 = 72
							0, 56, 			// min limits, yMin = 3*16 +8 = 56 
							size.x * GV.k, 120); // max limits, yMax = 5*16 +8 +32= 120
		player.setVelocityStep(0, GV.k);
		player.setVelocityBase(2, 0);
		add(player);
		
		// add follow cam and stick it to the player + passing world size (in px)
		followCam = new FollowCam(player, size.x * GV.k, size.y * GV.k);
		followCam.lockY(Std.int(-GV.k/2));
		add(followCam);
		
		//triggers
		trigger = new MonkeysTrigger(150 * GV.k, 72);
		add(trigger);
		add(new EnterInJet(247 * GV.k, 72));
		
		HXP.screen.color = 0x38e1e9; // sky color
		
		//texts
		add(new InfoText("Arrow to move", 10, 0));
		add(new InfoText("Spacebar to shoot", 40, 0));
		add(new InfoText("Level 1\n GO EAST!", 70, 0, "launchMusic"));
		add(new InfoText("Grab the burger", 130, 0));
		add(new InfoText("Enter the jet", 220, 0, "deactiveMonkeysTrigger"));
		add(new InfoText("", 235, 0, "mutePlayer"));
		
		// music  
		if(GV.music == null) 
			GV.music = new Sfx("sfx/Beam Me Up.mp3");
		
		if (GV.music != null && GV.music.playing)
			GV.music.stop();
	}
	
	function launchMusic():Void
	{
		GV.music.loop();
	}
	
	function deactiveMonkeysTrigger():Void 
	{
		trigger.active = false;
	}
	
	function mutePlayer():Void 
	{
		player.muteLaser(true);
	}
	
}