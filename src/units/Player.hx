package src.units;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import defines.GV;
import elements.Laser;
import flash.geom.Point;
import flash.geom.Rectangle;
import utils.Color;
import gui.InfoText;
import com.haxepunk.Sfx;
import levels.Fail;


/**
 * ...
 * @author Godjam - QilinEggs
 */
class Player extends Entity
{
	private var sprite:Spritemap;
	private var velocity:Point;
	private var coolDown:Float;
	private var coolDownValue:Float;
	private var limitsMin:Point;
	private var limitsMax:Point;
	private var velocityStep:Point; // velocity increment
	private var velocityBase:Point; // base velocity
	private var friction:Float;
	private var acceleration:Float;
	private var hp:Float;
	private var forcedSpriteAnim:String;
	private var mute:Bool;
	private var sunglasses:Image;
	
	public function new(x:Float, y:Float, xMin:Float, yMin:Float, xMax:Float, yMax:Float) 
	{
		super(x, y);
		
		// create the hero spritemap
        sprite = new Spritemap("gfx/WestHero.png", 14, 32);
		setHitbox(12, 32);
        // animations : idle
        sprite.add(GV.idleAnim, [0]);
        // walk
        sprite.add(GV.walkAnim, [1, 2, 3, 2], 5);
		// jet
		sprite.add(GV.jetAnim, [4]);
		// fly
		sprite.add(GV.flyAnim, [5, 6], 5);
		
        // play the idle animation by default
        sprite.play(GV.idleAnim);
		// no anim forced
		forcedSpriteAnim = "";
		
		graphic = sprite;
		layer = GV.layerPlayer;
 
		// setup initial velocity
		velocity = new Point();
		
		// laser cooldown
		coolDown = 0;
		coolDownValue = 1;
		mute = false;
		
        // defines left and right as arrow keys and ZQSD controls
        Input.define("left", 	[Key.LEFT, 	Key.Q]);
        Input.define("right", 	[Key.RIGHT, Key.D]);
		Input.define("up", 		[Key.UP, 	Key.Z]);
        Input.define("down", 	[Key.DOWN, 	Key.S]);
		Input.define("shoot", 	[Key.SPACE, Key.E]);
		
		// collision type
		type = GV.playerTag;
		
		//limits
		limitsMin = new Point(xMin, yMin);
		limitsMax = new Point(xMax, yMax);
		velocityStep = new Point(2, 2);
		velocityBase = new Point(0, 0);
		friction = 1;
		acceleration = 0;
		
		// hp
		hp = 10;
	}
	
	public function setVelocityStep(xStep:Float, yStep:Float) 
	{
		velocityStep = new Point(xStep, yStep);
	}
	
	public function setVelocityBase(xBase:Float, yBase:Float) 
	{
		velocityBase = new Point(xBase, yBase);
	}
	
	// keyboard input : velocity and shooting
    private function handleInput()
    {
        velocity.x = HXP.lerp(velocity.x, velocityBase.x, friction);
		velocity.y = HXP.lerp(velocity.y, velocityBase.y, friction);
 
		// check for continuous watching
        if (Input.check("left")) 
        {
            velocity.x -= velocityStep.x;
			// limit speed
			if (velocity.x <  -velocityStep.x && velocityStep.x != 0) velocity.x = -velocityStep.x;
        }
 
        if (Input.check("right"))
        {
            velocity.x += velocityStep.x;
			// limit speed
			if (velocity.x >  velocityStep.x && velocityStep.x != 0) velocity.x = velocityStep.x;
        }
		
        if (Input.pressed("up"))
        {
            velocity.y -= velocityStep.y;
        }
		
        if (Input.pressed("down"))
        {
            velocity.y += velocityStep.y;
        }
		
		if (Input.check("shoot"))
		{
			shoot();
		}
		
		// increase Y speed if needed
		velocityBase.y += HXP.elapsed * acceleration;
    }
	
	private function setAnimations()
    {
		// forced anim
		if (forcedSpriteAnim.length != 0)
			sprite.play(forcedSpriteAnim);
		
		// stopped, set animation to idle
        else if (velocity.length == 0) {
            sprite.play(GV.idleAnim);
        }
		
		// moving, set animation to walk
        else {
            sprite.play(GV.walkAnim);
        }
		
		// special cases : flyAnim 
		if (sprite.currentAnim == GV.flyAnim) {
			// flip on X
			if (velocity.x < 0)
				sprite.flipped = true;
			else
				sprite.flipped = false;
		}
    }
	
	private function shoot() {
		if (coolDown <= 0 && mute == false) {
			if(velocityBase.x > 0)
				scene.add(new Laser(x + width, y + height / 8));
			else if(velocityBase.y < 0)
				scene.add(new Laser(x + halfWidth, y - height - 32, 0, -1));
			
			new Sfx("sfx/player_shoot.mp3").play();
			coolDown = coolDownValue;
			GV.camVibrator.vibrate(0.4);
			Color.randomSkyColor();
		}
	}
	
	private function move()
	{
		if ((x + velocity.x < limitsMin.x && velocity.x<0)|| (x+velocity.x+width > limitsMax.x && velocity.x>0))
			velocity.x = 0;
			
		if ((y + velocity.y < limitsMin.y && velocity.y<0) || (y+velocity.y+height > limitsMax.y && velocity.y>0))
			velocity.y = 0;
			
		moveBy(velocity.x, velocity.y);
	}
	
	private function collideTest()
    {
		var e:Entity = collide(GV.enemyTag, x, y);
		if (e != null) {
			scene.remove(e);
			hit();
		}
	}
	
    override public function update()
    {
		// if alive 
		if(hp > 0) {
			// moove and shoot 
			handleInput();
			move();
			
			// collision test
			collideTest();
			
			// set current animation 
			setAnimations();
			
			// regen 
			regen();
		}
		
		//if dead & cd is done
		else if (coolDown < 0)
			HXP.scene = new Fail();
		
		//update cooldown 
		coolDown -= HXP.elapsed;
		
		super.update();
    }
	
	private function regen()
	{
		if(hp >0 && hp < 10)
			hp += 0.01;
	
		GV.hud.updateLife(Std.int(hp));
	}
	
	public function hit(dammage:Int= 1) 
	{
		hp -= dammage;
		// sfx
		new Sfx("sfx/player_hit.mp3").play();
		
		//update hud
		GV.hud.updateLife(Std.int(hp));
		
		if (hp <= 0) {
			GV.explosion.explode(centerX, centerY);
			scene.add(new InfoText("You fail!"));
			new Sfx("sfx/big_explosion.mp3");
			graphic = null;
			coolDown = 3; //3 sec before quit the scene and go to the lose scene
		}
	}
	
	public function boost()
	{
		coolDownValue = 0.2; // X5 !
		coolDown = 0;
	}
	
	public function forceSpriteAnim(anim:String, x:Float =-1, y:Float =-1) 
	{
		this.forcedSpriteAnim = anim;
		if (x != -1) this.x = x;
		if (y != -1) this.y = y;
	}
	
	public function setFrictionAndAccel(friction:Float=1, accel:Float=0) 
	{
		this.friction = friction;
		this.acceleration = accel;
	}
	
	public function muteLaser(mute:Bool) 
	{
		this.mute = mute;
	}
	
	public function setYMin(yMin:Float)
	{
		limitsMin.y = yMin;
	}
	
	public function setSunglasses()
	{
		sunglasses = new Image("gfx/sunGlasses.png", new Rectangle(9, 0, 9, 4));
		sunglasses.x = 5;
		sunglasses.y = 3;
		addGraphic(sunglasses);
	}
}