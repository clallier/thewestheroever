package src.units;


import com.haxepunk.graphics.Spritemap;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class GroundMonkey extends Enemy
{

	override public function new(x:Float, y:Float) 
	{
		super(x, y);
		sprite = new Spritemap("gfx/GroundMonkey.png", 12, 16);
		setHitbox(12, 16);
		sprite.add("walk", [1, 2], 5);
		sprite.play("walk");
		graphic = sprite;
	}
	
	override private function move() 
	{
		moveBy(-1, 0);
	}
}