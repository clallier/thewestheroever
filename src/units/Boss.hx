package units;

import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import com.haxepunk.Sfx;
import defines.GV;
import gui.InfoText;
import levels.Fail;
import src.units.Enemy;
import src.units.SpaceMonkey;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Boss extends Enemy
{
	private var dX:Float;
	private var dY:Float;
	private var timeout:Float;
	private var invincible:Bool;
	private var xSpeed:Float;
	private var baseTimeout:Float; 
	private var escaped:Bool;
	
	public function new(x:Float, y:Float) 
	{
		super(x, y);
		
		// init cost
		cost = 10000;
		// life
		hp = 100;
		GV.hud.updateBossLife(hp);
		
		sprite = new Spritemap("gfx/DK.png", 32, 32);
		// graphic setup
		setHitbox(32, 24, 4);
		graphic = sprite;
		layer = GV.layerBoss;
		
		// mvt
		dX = Math.random() * (10.5 * GV.k);
		dY = -height;
		
		// timeout
		timeout = 5;
		invincible = true;
		baseTimeout = 1;
		escaped = false;
		
		//x pos lerp speed
		xSpeed = 0.05;
	}
	
	override public function hit() {
		if (invincible)
			return;
		
		super.hit();
		GV.hud.updateBossLife(hp);
	}
	
	override private function move() 
	{
		// if enter in escape zone 
		if (y < 50 * GV.k) escape();
		// normal move
		else changePosition();
	}	
	
	override private function shoot() 
	{
		if (timeout < 0 && Math.abs(dX-x) < 2) {
			
			// Pop 3 new space monkeys
			for (i in 0...3)  
				scene.add(new SpaceMonkey(x, y+halfHeight));
			
			timeout = baseTimeout;
			
			// stop invicibility
			if(invincible)
				invincible = false;
		}
		
		timeout -= HXP.elapsed;
	}
	
	function changePosition():Void 
	{
		// take new target position 
		if (Math.abs(dX-x) < 2) {
			dX = Math.random() * (10.5 * GV.k);
			if (dX > x)
				sprite.flipped = true;
			else
				sprite.flipped = false;
		}
		
		if (dY != 5)
			dY = HXP.lerp(dY, 5, 0.03);
			
		
		x = HXP.lerp(x, dX, xSpeed);
		y = HXP.camera.y + dY;
	}
	
	function escape():Void 
	{	
		// try to escape
		if(escaped == false) {
			dY = HXP.lerp(dY, -(height+8), 0.05);
			dX = x +10; //no shoot
			y = HXP.camera.y + dY;
		
			
			if (y+height < HXP.camera.y && escaped == false ) {
				scene.add(new InfoText("The Boss has Escaped\nYou lose", 0, y/ GV.k));
				invincible = true;	
				escaped = true;
				timeout = 3;
			}
		}
	}
	
	public function boost() {
		xSpeed = 0.1; // X2 !
		baseTimeout = 0.5;
	}
	
	override function death() : Void
	{
		//remove itself
		scene.remove(this);
		// emit particles
		GV.explosion.explode(centerX, centerY);
		// update score
		GV.hud.incrementScore(Std.int(cost));
		// sfx
		new Sfx("sfx/big_explosion.mp3").play();
		
		// launch next level
		Reflect.callMethod(scene, Reflect.field(scene, "gotoLevel3"), []);
	}	
	
	override public function checkForVisibility() 
	{
		// do nothing
	}
	
	override public function update() 
	{
		super.update();
		// lose
		if (escaped == true) {
			graphic = null;
			if (timeout < 0) HXP.scene = new Fail();
		}
		
		// death 
		if(hp <= 0){
			GV.explosion.explode(centerX, centerY);
		}
	}
}