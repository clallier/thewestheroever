package src.units;

import com.haxepunk.Entity;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Spritemap;
import defines.GV;
import com.haxepunk.Sfx;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Enemy extends Entity
{
	private var hp:Int;
	private var timeLived:Float;
	private var sprite:Spritemap;
	private var cost:Float;
	
	private function new(x:Float, y:Float) 
	{
		super(x, y);
		
		// base HP
		hp = 1;
		// set type for collision
		type = GV.enemyTag;
		// init time lived
		timeLived = 0;
		// init cost
		cost = 100;
		
		layer = GV.layerEnemy;
	}
	
	// each enemy type should define this 
	private function move() {};
	private function shoot() {};
	
	function death():Void 
	{
		scene.remove(this);
		
		// emit particles
		GV.explosion.explode(centerX, centerY);
		// update score
		GV.hud.incrementScore(Std.int(cost));
		// sfx
		new Sfx("sfx/monkey_explode.mp3").play();

	}
	
	
	override public function update() 
	{
		// checkif unit is dead 
		if ( !isDead()) {
			
			// if not move and shoot 
			move();
			shoot();
			checkForVisibility();
			timeLived += HXP.elapsed;
			cost *= 0.999; // lost 0.01% value
		} else 
			death();
	}
	
	/*
	 * Hit : it lost an HP
	 * */
	public function hit() 
	{
		hp--;
		new Sfx("sfx/monkey_hit.mp3").play();
	}
	
	public function isDead() : Bool
	{
		if (hp <= 0)
			return true;
		
		return false;
	}
	
	public function checkForVisibility() 
	{
		// remove unit only if it's aged of 10 s and is no more visible
 		if (onCamera == false && timeLived > 10) 
			scene.remove(this);
	}
}