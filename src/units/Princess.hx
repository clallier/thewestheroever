package units;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import defines.GV;
import gui.HUD;
import gui.InfoText;
import src.units.Player;
import utils.Color;
import com.haxepunk.Sfx;


/**
 * ...
 * @author Godjam - QilinEggs
 */
class Princess extends Entity
{

	private var hero:Entity;
	private var sprite:Spritemap;
	
	public function new(x:Float=0, y:Float=0) 
	{
		super(x, y);
		sprite = new Spritemap("gfx/princess.png", 14, 32);
		setHitbox(12, 16, 1, 16);
        // animations : idle
        sprite.add(GV.idleAnim, [0]);
        // fly
        sprite.add(GV.flyAnim, [1, 2], 5);
		sprite.play(GV.flyAnim);
	
		// graphic
		graphic = sprite;
		layer = GV.layerEnemy; // overlapping with the hero
		
		//hero 
		hero = null;
	}
	
	
	override public function update() {
		
		// no hero 
		if(hero == null) { 
			moveBy(0, 2, GV.playerTag);	
		}
		
		// with hero
		else {
			var xOld:Float = x;
			x = hero.x + 2;
			y = hero.y - 1; 
			
			// x flip
			if(x-xOld < 0)
				sprite.flipped = false;
			else 
				sprite.flipped = true;
		}
		
		// destroy 
		if (y <= HXP.camera.y - 10) {
			scene.remove(this);
			GV.explosion.explode(x, y); 
			scene.add(new InfoText("You fail to save\nthe princess!", 0,  y / GV.k));
		}
	}
	
	override public function moveCollideY(e:Entity):Bool {
		// we collide with the player (only once)
		if (e != null && hero == null) {
			
			// Change sky color 
			Color.randomSkyColor();
			
			// up score
			GV.hud.incrementScore(3500); // you got the princess !
			
			// add text 
			scene.add(new InfoText("Awesome!", 0, y / GV.k));
			
			// sfx
			new Sfx("sfx/awesome.mp3").play();
			
			// follow the hero
			hero = e;
			
			// save it
			GV.hud.setPrincess(true);
			
			return true;
		}
		return false;
	}
	
	public function setAnim(anim:String) 
	{
		sprite.play(anim);
	}
}