package src.units;

import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import elements.Laser;
import com.haxepunk.Sfx;
import defines.GV;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class SpaceMonkey extends Enemy
{

	private var dX:Float;
	private var dY:Float;
	private var coolDown:Float;
	
	override public function new(x:Float, y:Float) 
	{
		super(x, y);
		sprite = new Spritemap("gfx/SpaceMonkey.png", 12, 16);
		sprite.add(GV.flyAnim, [1, 2], 5);
		sprite.play(GV.flyAnim);
		setHitbox(12, 16);
		graphic = sprite;
		
		dX = Math.random() * (HXP.width - this.width);
		dY = y - HXP.camera.y;
		coolDown = 4 + Math.random();
	}
	
	override private function move() 
	{
		// take new target position 
		if (dX == x) 
			dX = Math.random()*(HXP.width-this.width);
		
		x = HXP.lerp(x, dX, 0.001);
		y = HXP.camera.y + dY + timeLived*3;
		//HXP.log(dX + " " + x);
	}
	
	override private function shoot() 
	{
		if (coolDown < 0) {
			//add laser
			scene.add(new Laser(x, y + height, 0, 1, 10));
			// sfx
			new Sfx("sfx/monkey_shoot.mp3").play();
			// re set cooldown 
			coolDown = 2;
		}
		
		coolDown -= HXP.elapsed;
	}	
}